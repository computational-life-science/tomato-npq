#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Functions to simulate the measurement data from MultispeQ adapted from Matuszynska et al 2016

PhotosynQ protocol used: RIDES Light Potential PRL 1 
https://photosynq.org/protocols/rides-light-potential-prl-1



"""
import numpy as np
from modelbase.ode import Simulator

   
def changingLight(model, y0d, PFD=125.):
    r"""Performs light simulation for given light intensity. The function is designed to be as similar as possible to the "real" measurement process - so it has a huge amount of pulse (690 pulses in total). Further optimization is needed to enhance the runtime.


    
    Parameters
    ----------
    model : modelbase object
    y0d : numpy.array or list
        initial concentrations of compounds
    PFD : int
        light intensity
    

    Returns
    -------
    s : modelbase object
        contains results of the simulation
     
    """
    tprot = np.array(([1.5e-3, 3e-5]*100 + [1.5e-3, 2e-5]*130) 
                     + [10] +([1.5e-3, 3e-5]*100 + [1.5e-3, 2e-5]*130) 
                     + [10] +([1.5e-3, 3e-5]*100 + [1.5e-3, 2e-5]*130))
    
    ProtPFDs = np.array(([PFD, PFD]*100 + [PFD, 0.] * 20 + [PFD, PFD] * 30 + [PFD, 8000.] * 30
                        + [PFD, 6000.] * 15+ [PFD, 5000.] * 15+ [PFD, 0.] * 10+ [PFD, PFD] * 10) 
                        + [2000, 2000]
                        + ([2000, PFD]*100 + [2000, 0.] * 20 + [2000, PFD] * 30 + [2000, 8000.] * 30
                        + [2000, 6000.] * 15+ [2000, 5000.] * 15+ [2000, 0.] * 10+ [2000, PFD] * 10)
                        + [0., 0.]
                        + ([0., PFD]*100 + [0., 0.] * 20 + [0, PFD] * 30 + [0, 8000.] * 30
                        + [0., 6000.] * 15+ [0., 5000.] * 15+ [0., 0.] * 10+ [0.1, PFD] * 10))
    
    
    s = Simulator(model)
    s.initialise(y0d)
    dt = 0
    for i in range(len(tprot)):
        s.update_parameter('pfd', ProtPFDs[i])
        dt += tprot[i]
        s.simulate(dt, **{"rtol":1e-14,"atol":1e-10, "maxnef": 20, "maxncf":10})
    return s

#################################################

#Function to calculate NPQt 

def cal_NPQt(F, t, lights, maxlight=8000):
    r"""Calculates the NPQt from the extracted important points of the PAM simulations - the NPQt formula is taken from  Tietz et al. 2017
    https://doi.org/10.1111/pce.12924


    Parameters
    ----------
    F : numpy.array or list
        Simulated fluorescence values of PAM experiment
    t : numpy.array or list
        Time points 
    lights : numpy.array or list
        PFD values for each time point
    maxlight : int 
       The highest value of PFD of saturating light impuls
    

    Returns
    -------
    NPQt0 : float
        Calculated NPQt0 values
    NPQt1 : float
        Calculated NPQt1 values
    NPQt2 : float
        Calculated NPQt2 values
     
    """
    z0 = [] 
    o0 = [] #container for position of Fo'
    z1 = []
    o1 = []
    z2 = []
    o2 = []
    cnt = 0
    while cnt < len(lights):
        if lights[cnt] == maxlight:
            if lights[cnt-1]==2000:
                h1 = [] #temporary container for all peaks measured in full sunlight condition. For each peak it is renewed
                while cnt != len(lights) and lights[cnt] == maxlight:
                    h1.append(cnt)
                    cnt += 1
                z1.append(h1)      #container for lists. Each list contains the positions of fluorescence values for one peak 
                o1.append(h1[0]-1) #value directly at the bottom of peak is F'o
            else:
                if lights[cnt-1]==0:
                    h2 = [] #temporary container for all peaks measured in dark condition. 
                    while cnt != len(lights) and lights[cnt] == maxlight:
                        h2.append(cnt)
                        cnt += 1
                    z2.append(h2)
                    o2.append(h2[0]-1) 
                else:
                    h0 = [] #temporary container for  all peaks measured in ambient light condition. 
                    while cnt != len(lights) and lights[cnt] == maxlight:
                        h0.append(cnt)
                        cnt += 1
                    z0.append(h0)
                    o0.append(h0[0]-1) #
        else:
            cnt += 1
    peaks1 = [i[np.argmax(F[i])] for i in z1] #Fm'  is the maximal value for each peak sequence
    Fmp1 = max(F[peaks1])
    Fop1 = min(F[o1])
    NPQt_1 = (4.88/(Fmp1/Fop1 - 1))-1 #NPQt_1 is calculated from the peaks measured in full sunlight condition.
    
    peaks2 = [i[np.argmax(F[i])] for i in z2] 
    Fmp2 = max(F[peaks2])
    Fop2 = min(F[o2])
    NPQt_2 = (4.88/(Fmp2/Fop2 - 1))-1 #NPQt_2 is calculated from the peaks measured in dark condition.
    
    peaks0 = [i[np.argmax(F[i])] for i in z0] 
    Fmp0 = max(F[peaks0])
    Fop0 = min(F[o0])
    NPQt_0 = (4.88/(Fmp0/Fop0 - 1))-1 #NPQt_1 is calculated from the peaks measured in ambient light condition,
    
    return  NPQt_0, NPQt_1, NPQt_2  

##############################################

#create new function calculate NPQt based on input PFD
def NPQt_list(model, y0, datPFDs):
    r"""Calculates the NPQt with the input PFD value


    Parameters
    ----------
    model : modelbase object
    y0 : numpy.array or list
        initial concentrations of compounds
    PFD : int
        light intensity


    Returns
    -------
    box0 : list
        List of calculated NPQt0 values
    box0 : list
        List of calculated NPQt1 values
    box0 : list
        List of calculated NPQt2 values
     
    """
    box0 = []
    box1 = []
    box2 = []
    s = Simulator(model)
    s.initialise(y0)
    for i in range(len(datPFDs)):
        s.update_parameters({'pfd': datPFDs[i]})
        s.simulate(1000, steps=3000, **{"atol":1e-8})
        y0d = s.get_results_array()[-1]


        PAM4 = changingLight(model, y0d, PFD=datPFDs[i])
        NPQt_0, NPQt_1, NPQt_2  = cal_NPQt(PAM4.get_variable('Fluo'), PAM4.get_time(), PAM4.get_variable('L'), 8000)
        box0.append(NPQt_0)
        box1.append(NPQt_1)
        box2.append(NPQt_2)
    return box0, box1, box2
