#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
July 2023 - Adjusted for tomato KEA3


NPQ model for tomato - adapted from the original NPQ model for Arabidopsis, with new implement from the NPQ model for diatom


"""


import numpy as np
from modelbase.ode import Model
import matplotlib.pyplot as plt

plt.style.use('ggplot')

#Comments from original implementation by Matuszynska et al 2016 

pars = {
            
    #Pool sizes 
    'PSIItot': 2.5,  # unchanged [mmol/molChl] total concentration of PSII        
    'PQtot': 20.,     # unchanged [mmol/molChl]      
    'APtot': 50.,     # unchanged [mmol/molChl] Bionumbers ~2.55mM (=81mmol/molChl)      
    'PsbStot': 1,    # [relative] LHCs that get phosphorylated and protonated    
    'Xtot': 1.,       # unchanged [relative] xanthophylls     
    'O2ex': 8.,       # unchanged external oxygen, kept constant, corresponds to 250 microM, corr. to 20%      
    'Pi': 0.01,      # unchanged       
    
    'kkea': 1./3,   # new partitioning of protons for a WT - NEW PARAMETER
            
    #Rate constants and key parameters
    'kCytb6f': 0.104,          # unchanged a rough estimate of the transfer from PQ to cyt that is equal to ~ 10ms
                               # [1/s*(mmol/(s*m^2))] - gets multiplied by light to determine rate
    'kActATPase': 0.01,        # unchangedparamter relating the rate constant of activation of the ATPase in the light
    'kDeactATPase': 0.002,     # unchangedparamter relating the deactivation of the ATPase at night
    'kATPsynthase': 20.,       # unchanged
    'kATPconsumption': 10.,    # unchanged
    'kPQred': 250.,            # unchanged [1/(s*(mmol/molChl))]
    'kH': 5e9,                 # unchanged
    'kF': 6.25e8,              # unchanged fluorescence 16ns
    'kP': 5e9,                 # unchanged 
    'pHstroma': 7.8,           # unchanged [1/s] leakage rate
    'kleak': 1000.,            # unchanged
    'bH': 100,                 # unchanged proton buffer: ratio total / free protons
    'HPR': 14./3.,             # unchanged
            
    #Parameter associated with xanthophyll cycle
    'kDeepoxV': 0.0024,
    'kEpoxZ': 0.00024,         # 6.e-4,  #converted to [1/s]
    'KphSatZ': 6.3,            # changed based on ye experimental evidence
                               # [-] half-saturation pH value for activity de-epoxidase, highest activity at ~pH 5.8
    'Kzsat': 0.12,             # [-], half-saturation constant (relative conc. of Z) for quenching of Z
    'nHX': 5.,                 # unchanged, the cooperativity, hill-coefficient for activity of de-epoxidase
    'ketc': 1.,                # factor to change the speed of epoxidise under limiting light conditions - change to 2 - Jul 2023
            
     #Parameter associated with PsbS protonation
    'nHL': 3,
    'kDeprot': 0.0096,
    'kProt': 0.0096,
    'KphSatLHC': 5.8,
            
    #Fitted quencher contribution factors
    'gamma0': 0.1,   # slow quenching of Vx present despite lack of protonation
    'gamma1': 0.25,  # fast quenching present due to the protonation
    'gamma2': 0.6,   # slow quenching of Zx present despite lack of protonation
    'gamma3': 0.15,  # fastest possible quenching
    
    #Physical constants
    'F': 96.485,  # unchanged Faraday constant
    'R': 8.3e-3,  # unchanged universal gas constant
    'T': 298,     # unchanged Temperature in K - for now assumed to be constant at 25 C
            
    #Standard potentials and DG0ATP
    'E0QAQAm': -0.140,         # unchanged
    'E0PQPQH2': 0.354,         # unchanged
    'E0PCPCm': 0.380,          # unchanged
    'DG0ATP': 30.6,            # unchanged [kJ/mol / RT]
            
    #PFD
    'pfd': 0.                  # reference value set to 0.
            }

############################################################

def calculate_pHinv(x):
    ''' change back to 2016 model'''
    return 4e3 * 10 ** -x

def calculate_pH(H):
    "change back to 2016 model"
    return -np.log10(H*2.5e-4)
############################################################

# define the basic model 
model_tom = Model(pars)

############################################################

# add compounds 
model_tom.add_compounds([
    "P", # reduced Plastoquinone
    "H", # luminal Protons
    "E", # ATPactivity
    "A", # ATP
    "Pr", # fraction of non-protonated PsbS (notation from doctoral thesis Matuszynska 2016)
    "V" # fraction of Violaxanthin
    ])
    

############################################################
    
model_tom.add_derived_parameter(
    parameter_name="RT", function=lambda r, t: r * t, parameters=["R", "T"])

def _KeqQAPQ(F, E0QAQAm, E0PQPQH2, pHstroma, RT):
    DG1 =  -F*E0QAQAm 
    DG2 = -2*F*E0PQPQH2 + 2*pHstroma * np.log(10) * RT 
    DG0 = -2*DG1 + DG2
    Keq = np.exp(-DG0/RT)
    return Keq

model_tom.add_derived_parameter(
    parameter_name = "KeqQAPQ", function=_KeqQAPQ, parameters=["F", "E0QAQAm", "E0PQPQH2", "pHstroma", "RT"])

def ps2states(P, Q, light, PQtot, kPQred, KeqQAPQ, kH, kF, kP, PSIItot):
    """Calculates the states of photosystem II
    
    accepts:
    P: reduced fraction of PQ pool (PQH2)
    Q: Quencher
    
    returns:
    B: array of PSII states
    """
    
    Bs = []
    Pox = PQtot - P
    b0 = (light + kPQred*P/KeqQAPQ)
    b1 = (kH * Q + kF)
    b2 = kH * Q + kF + kP
    
    for Pox,b0,b1,b2 in zip(Pox,b0,b1,b2):
        A = np.array([
        [-b0,        b1,         kPQred*Pox,                0], #B0
        [light,     -b2,         0,                         0], #B1
        [0,          0,          light,                   -b1], #B3
        [1,          1,          1,                         1]
        ])
        
        b = np.array([0,0,0,PSIItot])
        B0,B1,B2,B3 = np.linalg.solve(A,b)
        Bs.append([B0, B1, B2, B3])
    return np.array(Bs).T #Why need to multiply with T?


def Keqcytb6f(H_beta, F, E0PQPQH2, RT, E0PCPCm, pHstroma):
    """Equilibrium constant of Cytochrome b6f"""
    DG1 = -2*F*E0PQPQH2 + 2 * RT * np.log(10) * calculate_pH(H_beta)
    DG2 = -F*E0PCPCm
    DG3 = RT*np.log(10)*(pHstroma - calculate_pH(H_beta))
    DG = -DG1 + 2*DG2 + 2*DG3
    Keq = np.exp(-DG/RT)
    return Keq
    
    
def KeqATPsyn(H, DG0ATP, pHstroma, RT, Pi):
    """Equilibrium constant of ATP synthase. For more
    information see Matuszynska et al 2016 or Ebenhöh et al. 2011,2014
    """
    DG = DG0ATP - np.log(10) * (pHstroma-calculate_pH(H)) * (14/3)  * RT
    Keq = Pi * np.exp(-DG/RT) 
    return Keq

def Fluorescence(P, Q, B0, B2, light, PQtot, kPQred, KeqQAPQ, kH, kF, kP, PSIItot):
    """Fluorescence function"""
    Fluo =  kF/(kH*Q + kF + kP) * B0 + kF/(kH*Q + kF) * B2
    return Fluo

def Quencher(Pr, V, Xtot, PsbStot, Kzsat, gamma0, gamma1, gamma2, gamma3):
    """Quencher mechanism
    
    accepts: 
    Pr: fraction of non-protonated PsbS protein
    V: fraction of Violaxanthin
    """
    Z = Xtot - V
    P = PsbStot - Pr
    Zs = Z/(Z +  Kzsat)
        
    Q = gamma0 * (1-Zs) * Pr \
    + gamma1 * (1-Zs) * P \
    + gamma2 * Zs * P \
    + gamma3 * Zs * Pr
    return Q

def kd(H, nHX, KphSatZ, kDeepoxV):
    """Deepoxidation of Vx"""
    a = H**nHX / (H**nHX + calculate_pHinv(KphSatZ)**nHX)
    v = kDeepoxV * a
    return v
####################################################


def pqmoiety(P, PQtot):
    return [PQtot - P]

def atpmoiety(A, APtot):
    return [APtot - A]

def psbsmoiety(Pr, PsbStot):
    return [PsbStot - Pr]

def xcycmoiety(X, Xtot):
    return [Xtot - X]

def hbeta(H, kkea):
    return kkea * H

###############################################

model_tom.add_algebraic_module(
        module_name = 'Proton_beta',
        function = hbeta,
        compounds = ["H"],
        derived_compounds = ['H_beta'],
        parameters = ['kkea']
              )

model_tom.add_algebraic_module(
    module_name="calculate_pH",
    function=calculate_pH,
    compounds=["H_beta"],
    derived_compounds=["pH"],
    modifiers=None
)


model_tom.add_algebraic_module(
    module_name = "P_am",
    function = pqmoiety,
    compounds = ["P"],
    derived_compounds = ["Pox"],
    parameters = ["PQtot"])


model_tom.add_algebraic_module(
    module_name = "A_am",
    function = atpmoiety,
    compounds = ["A"],
    derived_compounds = ["ADP"],
    parameters = ["APtot"])


model_tom.add_algebraic_module(
    module_name = "PsbS_am",
    function = psbsmoiety,
    compounds = ["Pr"],
    derived_compounds = ["Pnr"],
    parameters = ["PsbStot"])


model_tom.add_algebraic_module(
    module_name = "X_am",
    function = xcycmoiety,
    compounds = ["V"],
    derived_compounds = ["Z"],
    parameters = ["Xtot"])


model_tom.add_algebraic_module(
    module_name = "Quencher",
    function = Quencher,
    compounds = ["Pr", "V"],
    derived_compounds = ["Q"],
    parameters = ["Xtot", "PsbStot", "Kzsat",
                  "gamma0", "gamma1", "gamma2",
                  "gamma3"])


model_tom.add_algebraic_module(
    module_name = 'PSIIstates',
    function = ps2states,
    compounds = ["P", "Q"],
    derived_compounds = ["B0", "B1", "B2", "B3"],
    parameters = ["pfd", "PQtot", "kPQred",
                  "KeqQAPQ", "kH",  "kF", "kP", "PSIItot"])


model_tom.add_algebraic_module(
    module_name = "Fluorescence",
    function = Fluorescence,
    compounds = ["P", "Q", "B0", "B2"],
    derived_compounds = ["Fluo"],
    parameters = ['pfd', 'PQtot', 'kPQred', 'KeqQAPQ',
                  'kH',  'kF', 'kP', 'PSIItot'])
    
    
model_tom.add_algebraic_module(
    module_name = "Light",
    function = lambda X, PFD: PFD ,
    compounds = ["P"],
    derived_compounds = ["L"],
    parameters = ['pfd'])

model_tom.add_algebraic_module(
    module_name="kd",
    function=kd,
    compounds=["H_beta"],
    derived_compounds=["kd"],
    parameters=["nHX", "KphSatZ", "kDeepoxV"]
)

###################################################################


#========= REACTION RATES ============#    



def vps2(B1, kP): 
    """Reduction of PQ due to ps2"""
    v = kP * 0.5 * B1
    return v  
   



def vPQox(P, H_beta, light, kCytb6f, O2ex, PQtot, F, E0PQPQH2, RT, E0PCPCm, pHstroma):
    """Oxidation of the PQ pool through cytochrome and PTOX"""
    kPFD = kCytb6f * light
    Keq = Keqcytb6f(H_beta, F, E0PQPQH2, RT, E0PCPCm, pHstroma)
    a1 = kPFD * Keq/ (Keq + 1)  
    a2 = kPFD/(Keq + 1)
    v = a1 * P - a2 * (PQtot - P)
    return v


  
def vATPactivity(E, light,  kActATPase,  kDeactATPase):
    """Activation of ATPsynthase by light"""
    switch = light > 0. #?????
    v = kActATPase * switch * (1 - E) - kDeactATPase * (1-switch) * E
    return v




def vATPsynthase(H, A, E, pfd, kATPsynthase, DG0ATP, pHstroma, RT, Pi, APtot):
    """Production of ATP by ATPsynthase"""
    v = E * kATPsynthase * (APtot - A - A/KeqATPsyn(H, DG0ATP, pHstroma, RT, Pi))
    return v
    

    

def vATPcons(A, kATPconsumption):
    """ATP consuming reaction"""
    v = kATPconsumption * A
    return v




def vLeak(H, kleak, pHstroma):
    """Transmembrane proton leak"""
    v = kleak * (H - calculate_pHinv(pHstroma))
    return v






def vXdeepox(V, kd):
    """Deepoxidation of Vx"""
    v = kd * V
    return v

   
def vXepox(L, V, kEpoxZ, ketc, Xtot):
    """Expoxidation of Zx"""
    for l in L:
        if l<=50.:
            return  ketc * kEpoxZ * (Xtot - V)    
        else:
            return kEpoxZ * (Xtot - V)  

def vPsbSP(Pr, H, nHL, KphSatLHC, kProt, kDeprot, PsbStot):
    """Protonation of PsbS protein"""
    a = H**nHL / (H**nHL + calculate_pHinv(KphSatLHC)**nHL)
    v = kProt * a * Pr - kDeprot * (PsbStot - Pr)
    return v

    

###############################################################



model_tom.add_reaction(
        rate_name = "vXepox",
        function = vXepox,
        modifiers = ['L','V'],
        stoichiometry = {"V": 1},
        parameters = ["kEpoxZ", "ketc", "Xtot"])


model_tom.add_reaction(
        rate_name = "vPsbSP",
        function = vPsbSP,
        dynamic_variables= ["Pr", "H_beta"],
        stoichiometry = {"Pr":-1},
        parameters = ["nHL", "KphSatLHC", "kProt",
                      "kDeprot", "PsbStot"])

model_tom.add_reaction(
        rate_name = "vps2",
        function = vps2,
        modifiers=["B1"],
        stoichiometry = {"P":1,"H":2/pars["bH"]},
        parameters = ["kP"])

model_tom.add_reaction(
        rate_name = "vPQox",
        function = vPQox,
        modifiers = ['H_beta'],
        stoichiometry = {"P":-1,"H":4/pars["bH"]},
        parameters = ["pfd", "kCytb6f", "O2ex",
                          "PQtot", "F", "E0PQPQH2", "RT", "E0PCPCm",
                          "pHstroma"])

model_tom.add_reaction(
        rate_name = "vATPactivity",
        function = vATPactivity,
        modifiers = ["E"],
        stoichiometry = {"E":1},
        parameters = ["pfd",  "kActATPase",  "kDeactATPase"])

model_tom.add_reaction(
        rate_name = "vATPsynthase",
        function = vATPsynthase,
        modifiers = ["A","E"],
        stoichiometry = {"A":1, "H": (-14/3)/pars["bH"]},
        parameters = ["pfd", "kATPsynthase", "DG0ATP", "pHstroma",
                      "RT", "Pi", "APtot"])

model_tom.add_reaction(
        rate_name = "vATPcons",
        function = vATPcons,
        stoichiometry = {"A":-1},
        parameters = ["kATPconsumption"])

model_tom.add_reaction(
        rate_name = "vKEA3",
        function  = vLeak,
        stoichiometry = {"H": -1/pars["bH"]},
        parameters = ["kleak", "pHstroma"])      #Why H beta is not used to calculate this?

model_tom.add_reaction(
        rate_name = "vXdeepox",
        function = vXdeepox,
        modifiers = ['kd'],
        stoichiometry = {"V": -1},
        parameters = None)

