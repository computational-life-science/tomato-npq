# tomato-npq

Contain the model for NPQ process in tomato - Adapted from Seydoux et al. 2022 and Matuszynska et al. 2016 

Please use the yml file attached to activate the enironment for the model

- env_tomato.yml: the yml used for createing the conda enviroment to run the model
- model_tomato.py: Contain the tomato NPQ model - the first version, not accounted for temperature effect
- model_tomato_temp.py: Contain the tomato NPQ model - the first version, accounted for temperature effect
- functions.py: Contain the funtions to calculate NPQt value from the model and the function to simulate the light condition 
- Tomato.ipynb: notebook to recreate the NPQt calculation results